<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Project;

use Symfony\Component\HttpFoundation\BinaryFileResponse;

class Controller extends AbstractController
{
    /**
     * @Route("/", name="app_clement_vasques")
     */
    public function index(Request $request): Response
    {
        $manager = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Project::class);
        $allprojects = $repo->findAll();

        // set skills level
        $skills_dev = [
            ["Php", 8],
            ["Symfony & Twig", 8],
            ["Javascript", 8],
            ["Html & Css", 8],
            ["React", 6],
            ["Bootstrap", 7],
            ["Mysql", 4],
        ];

        $skills_webdesign = [
            ["Photoshop", 8],
            ["Illustrator", 8],
            ["Indesign", 6],
        ];


        return $this->render('/base.html.twig', [
            'projects'=>$allprojects,
            'skills_dev'=>$skills_dev,
            'skills_webdesign'=>$skills_webdesign
        ]);
    }

    /**
     * @Route("/projet/{id}", name="app_projet")
     * @param $id
     * 
     */
    public function project($id = null): Response
    {
        $manager = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Project::class);


        $allprojects = $repo->findAll();

        foreach($allprojects as $project){
            $allprojectsId[] = $project->getId();
        }

        // handle exception when user types a parameter that does not exist

        $firstIdProject = $allprojectsId[0];
        $lastIdProject = $allprojectsId[count($allprojectsId)-1];

        if ($id < $firstIdProject || $id > $lastIdProject){
            $id= $firstIdProject;
        }
        

        return $this->render('/project.html.twig', [
            "projects"=>$allprojects,
            "id"=>$id,
            "firstIdProject"=>$firstIdProject,
            "lastIdProject"=>$lastIdProject,
        ]);
    }

}
