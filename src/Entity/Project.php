<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $date_project;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $src_img;

    /**
     * @ORM\Column(type="text", length=1000)
     */
    private $content;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $langages = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $template_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $alt_img;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title_img;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDateProject(): ?string
    {
        return $this->date_project;
    }

    public function setDateProject(string $date_project): self
    {
        $this->date_project = $date_project;

        return $this;
    }

    public function getSrcImg(): ?string
    {
        return $this->src_img;
    }

    public function setSrcImg(string $src_img): self
    {
        $this->src_img = $src_img;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getLangages(): ?string
    {
        return $this->langages;
    }

    public function setLangages(string $langages): self
    {
        $this->langages = $langages;

        return $this;
    }

    public function getTemplateName(): ?string
    {
        return $this->template_name;
    }

    public function setTemplateName(string $template_name): self
    {
        $this->template_name = $template_name;

        return $this;
    }

    public function getAltImg(): ?string
    {
        return $this->alt_img;
    }

    public function setAltImg(string $alt_img): self
    {
        $this->alt_img = $alt_img;

        return $this;
    }

    public function getTitleImg(): ?string
    {
        return $this->title_img;
    }

    public function setTitleImg(string $title_img): self
    {
        $this->title_img = $title_img;

        return $this;
    }
}
