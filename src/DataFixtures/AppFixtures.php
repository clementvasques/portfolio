<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Project;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $projects = [
            ["name"=>"Créa Css",
            "date"=>"Février 2021",
            "langages"=>"Html5 Css3",
            "src_img"=>"img/crea_css.png",
            "content"=>"C'est un des premiers projets de la formation. Axé sur le fait d'apprendre comment les blocs se comportent en CSS, cette création personnelle fait comprendre (au moi graphiste) qu'il n'est pas si facile que ça de réaliser des illustrations sur le web. Le personnage m'a donné un peu de fil à retordre, mais je trouve ça sympa !",
            "template_name"=> "crea_css.html.twig",
            "alt_img"=>"projet réalisé avec le langage css et html",
            "title_img"=>"création css"],

            ["name"=>"Calculatrice",
            "date"=>"Février 2021",
            "langages"=>"Javascript Html5 Css3",
            "src_img"=>"img/calculatrice.png",
            "content"=>"Voici le premier exercice réalisé avec Javascript ! Initiation aux events, révision des positionnements en CSS : acquisition de solides bases pour la suite. Projet non fini au niveau algorithme, quelques bugs subsistent. Version disponible sur ordinateur et mobile.",
            "template_name"=> "calculatrice.html.twig",
            "alt_img"=>"projet réalisé avec le langage javascript",
            "title_img"=>"application calculatrice"],

            ["name"=>"Pong",
            "date"=>"Mars 2021",
            "langages"=>"Javascript Html5 Css3",
            "src_img"=>"img/pong.png",
            "content"=>"Cette remasterisation de l'indétronable classique jeu pong n'existe que grâce à un cours sur la manière d'animer des éléments en CSS. Pas censé être un projet de la formation, je me suis amusé à aller un peu plus loin en découvrant les event Javascript. Version uniquement disponible sur ordinateur / se joue à deux sur la même machine !",
            "template_name"=> "pong.html.twig",
            "alt_img"=>"projet réalisé avec le langage javascript",
            "title_img"=>"jeux pong"],

            ["name"=>"Mastermind",
             "date"=>"Mars 2021",
             "langages"=>"Javascript Html5 Css3",
             "src_img"=>"img/mastermind.png",
             "content"=>"Premier gros projet de la formation réalisé en Javascript natif. Nous avons entièrement conçu le projet par équipe de deux. Le design reflète plus ma pâte personnelle tandis que nous avons écrit l'algorithme à deux. Tout le code est écrit en dur et peut paraître assez indigeste à lire. Mais un peu d'auto-indulgence, seulement deux mois de formation : ça marche et c'est beau ! Version initialement adaptée pour ordinateur, mais fonctionnelle sur mobile. Amusez-vous !",
             "template_name"=> "mastermind.html.twig",
             "alt_img"=>"projet réalisé avec le langage javascript",
             "title_img"=>"jeux mastermind"],

             ["name"=>"Post It",
             "date"=>"Avril 2021",
             "langages"=>"Symfony Php Javascript Ajax Html5 Css3",
             "src_img"=>"img/postit.png",
             "content"=>"Premier gros projet back-end conceptualisé sous Symfony, Post It est un réseau social inspiré de Twitter. On peut y créer des posts et les commenter, les aimer. Nous avons également mis en place une partie authentification, qui oblige chaque utilisateur à s'inscrire fictivement. Chaque utilisateur a donc une page profil, avec les posts qu'il a écrits, combien de personnes le 'follow' etc. Création d'un système de notifications pour chaque 'event'. Réalisé avec l'ensemble des coéquipiers de la formation : mutualisation du code (groupe de 10/équipe de deux) via Git. Version disponible à venir.",
             "template_name"=> "post_it.html.twig",
             "alt_img"=>"projet réalisé avec le framework Symfony",
             "title_img"=>"application du réseau social post it"],

            ["name"=>"La bonne boite",
             "date"=>"Juillet 2021",
             "langages"=>"React Javascript Api_Rest Html5 Css3",
             "src_img"=>"img/labonneboite2.png",
             "content"=>"Application React se servant des données de l'API la bonne boite, ceci est le dernier projet de la formation ! Nouvelle manière de coder avec React, je trouve intéressant la façon dont cela permet de prendre du recul sur la façon d'écrire le code, en regardant 'l'état' de tel ou tel composant. Cumulé à l'apprentissage du fonctionnement d'une API Rest, super projet pour finir la formation en beauté ! Design mobile first. Version disponible à venir.",
             "template_name"=> "la_bonne_boite.html.twig",
             "alt_img"=>"projet réalisé avec le framework React",
             "title_img"=>"application web de recherche d'entreprises"],
        ];

        foreach ($projects as $newProject){
                $project = new Project();
                $project->setName($newProject["name"]);
                $project->setDateProject($newProject["date"]);
                $project->setLangages($newProject["langages"]);
                $project->setSrcImg($newProject["src_img"]);
                $project->setContent($newProject["content"]);
                $project->setTemplateName($newProject["template_name"]);
                $project->setAltImg($newProject["alt_img"]);
                $project->setTitleImg($newProject["title_img"]);

                $manager->persist($project);

        };

        $manager->flush();
    }
}
