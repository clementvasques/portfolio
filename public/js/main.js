// MOBILE NAVBAR


let hamburger = document.querySelector(".hamburger")
let navbar_mobile = document.querySelector(".navbar_mobile")
let cross = document.querySelector(".cross")
let link_mobile = document.querySelectorAll(".link_mobile")
let logo_mobile = document.querySelector(".logomobile")
let logo_mobileWithoutClick = document.querySelector(".container_logo")
// let navbar_mobile_Animation_Closing = document.querySelector('.navbar_mobile_Animation_Closing')

hamburger.addEventListener("click", function(){
    navbar_mobile.style.display = "flex"
    navbar_mobile.style.animationName = "animMobileNavbar"

    cross.style.display = "flex"
    hamburger.style.display= "none"
    logo_mobileWithoutClick.style.display = "none"

})


cross.addEventListener("click", function(){
    navbar_mobile.style.animationName = "navbar_mobile_Animation_Closing"


    hamburger.style.display = "flex"
    cross.style.display= "none"
    logo_mobileWithoutClick.style.display = "block"


})

for (link of link_mobile){
    link.addEventListener("click", function(){
        setTimeout(function(){
            navbar_mobile.style.display = "none"
            hamburger.style.display = "flex"
            logo_mobileWithoutClick.style.display = "flex"

            cross.style.display= "none"
        }, 50)
    })
}

logo_mobile.addEventListener('click', function(){
    navbar_mobile.style.display = "none"
    cross.style.display= "none"
    hamburger.style.display = "flex"
    logo_mobileWithoutClick.style.display = "flex"

})




// Get the viewport height and multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);


let viewHeightScreen = window.innerHeight
let viewWidthScreen = window.innerWidth

let html = document.querySelector("html")

if (viewHeightScreen / viewWidthScreen > 1.9 ){
}