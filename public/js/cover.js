let displayJunior = document.querySelector(".junior")
let textCoverImg2 = document.querySelector(".text_cover_img2")
let textCoverImg3 = document.querySelector(".text_cover_img3")
let textCoverImg1 = document.querySelector(".text_cover_img1")
let nextPhotoButton = document.querySelector(".nextButton")
let previousPhotoButton = document.querySelector(".previousButton")
let Image2 = document.querySelector(".block_image_2")
let Image1 = document.querySelector(".block_image")
let Image3 = document.querySelector(".block_image_3")
let progressBar = document.querySelector(".bar")


// --------------------------------------------
// WHEN PAGE IS LOADED, START FIRST ANIMATION
// --------------------------------------------

textCoverImg2.style.display = "none"
textCoverImg3.style.display = "none"
Image1.style.animation = "cover_zoom 10s"
let startedIntervalMilliSeconds = [new Date().getTime()]
let pausedIntervalMilliSeconds = [0]


function animation_progressBar (){
    progressBar.classList.remove("progress_bar")

    setTimeout(function (){
        progressBar.classList.add("progress_bar")
    }, 10)
}


let photoNumber = [1]

// ---------- STOP CLICK UNTIL ANIMATIONS OF EACH IMG AREN'T FINISH

let photoStopped = []

function hoverPhoto(){
    photoStopped[0] = false
    nextPhotoButton.style.opacity = "0.5"
    previousPhotoButton.style.opacity = "0.5"
    nextPhotoButton.style.cursor = "pointer"
    previousPhotoButton.style.cursor = "pointer"


    nextPhotoButton.addEventListener('mouseover', function (event){
        if (photoStopped[0] === false){
            event.target.style.opacity = "1"
        }
    })

    nextPhotoButton.addEventListener('mouseout', function (event){
        if (photoStopped[0] === false){
            event.target.style.opacity = "0.5"
        }
    })

    previousPhotoButton.addEventListener('mouseover', function (event){
        if (photoStopped[0] === false){
            event.target.style.opacity = "1"
        }
    })
    previousPhotoButton.addEventListener('mouseout', function (event){
        if (photoStopped[0] === false){
            event.target.style.opacity = "0.5"
        }
    })
}

function hoverPhotoStopped(){
    photoStopped[0] = true
    nextPhotoButton.style.opacity = "0.2"
    previousPhotoButton.style.opacity = "0.2"
    nextPhotoButton.style.cursor = "unset"
    previousPhotoButton.style.cursor = "unset"
}

setTimeout(function (){
    hoverPhotoStopped()
}, 0)

// -------- ACTIVATE HOVER AFTER THE END OF TEXTS ANIMATIONS
setTimeout(function (){
    hoverPhoto()
}, 2000)



// --------------------------------------------
// INTERVAL LOOP START
// --------------------------------------------


function next (){


    breaksCounter = 0
    timeLeftAfterFirstBreak = []
    timeLeftAfterSecondBreakAndMore = []
    newDateInMillisecondsAfterFirstBreak = []
    pausedIntervalMilliSeconds = []
    clearTimeout(ifThereIsNoMoreBreaksNextPhotos1)
    clearTimeout(ifThereIsNoMoreBreaksNextPhotos2)
    clearTimeout(ifThereIsNoMoreBreaksNextPhotos3)



    if (photoNumber[0] === 1){
        startedIntervalMilliSeconds.splice(0, 1, new Date().getTime()) 

        animation_progressBar()

        setTimeout(function (){
            hoverPhotoStopped()
        }, 0)

        setTimeout(function (){
            hoverPhoto()
        }, 2300)


        textCoverImg1.style.display = "none"
        textCoverImg2.style.display = "block"
        textCoverImg3.style.display = "none"
        displayJunior.style.opacity = "0"

        photoNumber.splice(0, 1, 2 )

        let getScale = getComputedStyle(Image1).transform.split(',')
        Image1.style.setProperty('--newScale', getScale[3])
        Image1.style.animation = "anim2 3s"

        Image2.style.opacity = "1"
        Image2.style.animation = "cover_zoom 10s"

        setTimeout(function (){
            Image1.style.opacity = "0"
        },3000)

    } else if (photoNumber[0] === 2){
        startedIntervalMilliSeconds.splice(0, 1, new Date().getTime())


        animation_progressBar()


        setTimeout(function (){
            hoverPhotoStopped()
        }, 0)

        setTimeout(function (){
            hoverPhoto()
        }, 2400)

        textCoverImg1.style.display = "none"
        textCoverImg2.style.display = "none"
        textCoverImg3.style.display = "block"
        displayJunior.style.opacity = "0"

        let getScale = getComputedStyle(Image2).transform.split(',')

        Image2.style.setProperty('--newScale', getScale[3])


        Image2.style.animation = "anim2 3s"

        Image3.style.opacity = "1"

        Image3.style.animation = "cover_zoom 10s"

        photoNumber.splice(0, 1, 3 )

        setTimeout(function (){
            Image2.style.opacity = "0"
            Image2.style.animation = "unset"

        },3000)
    } else if (photoNumber[0] === 3){
        startedIntervalMilliSeconds.splice(0, 1, new Date().getTime())


        animation_progressBar()

        setTimeout(function (){
            hoverPhotoStopped()
        }, 0)

        setTimeout(function (){
            hoverPhoto()
        }, 2000)

        displayJunior.style.display = "none"

        textCoverImg1.style.display = "block"
        textCoverImg2.style.display = "none"
        textCoverImg3.style.display = "none"

        let getScale = getComputedStyle(Image3).transform.split(',')

        Image3.style.setProperty('--newScale', getScale[3])

        Image3.style.animation = "anim2 3s"

        Image1.style.opacity = "1"

        photoNumber.splice(0, 1, 1 )
        Image1.style.animation = "anim 3s, cover_zoom 10s"

        displayJunior.style.display = "flex"

        setTimeout(function (){
            Image3.style.opacity = "0"
            displayJunior.style.opacity = "1"


        },3000)
    }

}

function previous(){
    breaksCounter = 0
    timeLeftAfterFirstBreak = []
    timeLeftAfterSecondBreakAndMore = []
    newDateInMillisecondsAfterFirstBreak = []
    pausedIntervalMilliSeconds = []
    clearTimeout(ifThereIsNoMoreBreaksNextPhotos1)
    clearTimeout(ifThereIsNoMoreBreaksNextPhotos2)
    clearTimeout(ifThereIsNoMoreBreaksNextPhotos3)
    
    if (photoNumber[0] === 1){
        animation_progressBar()

        setTimeout(function (){
            hoverPhotoStopped()
        }, 0)

        setTimeout(function (){
            hoverPhoto()
        }, 2400)

        textCoverImg1.style.display = "none"
        textCoverImg2.style.display = "none"
        textCoverImg3.style.display = "block"
        displayJunior.style.opacity = "0"

        photoNumber.splice(0, 1, 3 )

        let getScale = getComputedStyle(Image1).transform.split(',')
        Image1.style.setProperty('--newScale', getScale[3])
        Image1.style.animation = "anim2 3s"

        Image3.style.opacity = "1"
        Image3.style.animation = "cover_zoom 10s"

        Image2.style.opacity = "0"

        Image2.style.opacity = "-1"


        setTimeout(function (){
            Image1.style.opacity = "0"
            Image1.style.animation = "unset"
        },3000)

    } else if (photoNumber[0] === 2){
        animation_progressBar()

        setTimeout(function (){
            hoverPhotoStopped()
        }, 0)

        setTimeout(function (){
            hoverPhoto()
        }, 2000)
        displayJunior.style.display = "none"
        textCoverImg1.style.display = "block"
        textCoverImg2.style.display = "none"
        textCoverImg3.style.display = "none"

        Image1.style.zIndex = "-6"
        Image2.style.zIndex = "0"


        photoNumber.splice(0, 1, 1)
        let getScale = getComputedStyle(Image2).transform.split(',')
        Image2.style.setProperty('--newScale', getScale[3])
        Image2.style.animation = "anim2 3s"

        Image1.style.animation = "cover_zoom 10s"
        Image1.style.opacity = "1"

        displayJunior.style.display = "flex"


        setTimeout(function (){
            Image2.style.opacity = "0"
            displayJunior.style.opacity = "1"


            Image1.style.zIndex = "0"
            Image2.style.zIndex = "-1"

        },3000)


    } else if (photoNumber[0] === 3){
        animation_progressBar()

        setTimeout(function (){
            hoverPhotoStopped()
        }, 0)

        setTimeout(function (){
            hoverPhoto()
        }, 2300)



        textCoverImg2.style.display = "block"
        textCoverImg3.style.display = "none"

        photoNumber.splice(0, 1, 2)
        let getScale = getComputedStyle(Image3).transform.split(',')
        Image3.style.setProperty('--newScale', getScale[3])
        Image3.style.animation = "anim2 3s"

        Image2.style.animation = "cover_zoom 10s"
        Image2.style.opacity = "1"

        Image2.style.zIndex = "-6"


        setTimeout(function (){
            Image3.style.opacity = "0"
            Image2.style.zIndex = "-1"
        },3000)

    }
}


let globalInterval = setInterval(function (){
    startedIntervalMilliSeconds.splice(0, 1, new Date().getTime())

    if (photoNumber[0] === 1){
        animation_progressBar()

        setTimeout(function (){
            hoverPhotoStopped()
        }, 0)

        setTimeout(function (){
            hoverPhoto()
        }, 2300)


        textCoverImg1.style.display = "none"
        textCoverImg2.style.display = "block"
        textCoverImg3.style.display = "none"
        displayJunior.style.opacity = "0"

        photoNumber.splice(0, 1, 2 )

        let getScale = getComputedStyle(Image1).transform.split(',')
        Image1.style.setProperty('--newScale', getScale[3])
        Image1.style.animation = "anim2 3s"


        Image2.style.opacity = "1"

        Image2.style.animation = "cover_zoom 10s"

        setTimeout(function (){
            Image1.style.opacity = "0"

        },3000)

    } else if (photoNumber[0] === 2){
        animation_progressBar()


        setTimeout(function (){
            hoverPhotoStopped()
        }, 0)

        setTimeout(function (){
            hoverPhoto()
        }, 2400)

        textCoverImg1.style.display = "none"
        textCoverImg2.style.display = "none"
        textCoverImg3.style.display = "block"
        displayJunior.style.opacity = "0"

        let getScale = getComputedStyle(Image2).transform.split(',')

        Image2.style.setProperty('--newScale', getScale[3])


        Image2.style.animation = "anim2 3s"

        Image3.style.opacity = "1"

        Image3.style.animation = "cover_zoom 10s"

        photoNumber.splice(0, 1, 3 )

        setTimeout(function (){
            Image2.style.opacity = "0"
            Image2.style.animation = "unset"

        },3000)
    } else if (photoNumber[0] === 3){
        animation_progressBar()

        setTimeout(function (){
            hoverPhotoStopped()
        }, 0)

        setTimeout(function (){
            hoverPhoto()
        }, 2000)

        displayJunior.style.display = "none"


        textCoverImg1.style.display = "block"
        textCoverImg2.style.display = "none"
        textCoverImg3.style.display = "none"

        let getScale = getComputedStyle(Image3).transform.split(',')

        Image3.style.setProperty('--newScale', getScale[3])

        Image3.style.animation = "anim2 3s"

        Image1.style.opacity = "1"

        photoNumber.splice(0, 1, 1 )
        Image1.style.animation = "anim 3s, cover_zoom 10s"

        displayJunior.style.display = "flex"

        setTimeout(function (){
            Image3.style.opacity = "0"
            displayJunior.style.opacity = "1"
        },3000)
    }

}, 10000)


// --------------------------------------------
// VARIABLE DECLARATION THAT WILL BE USED TO RESTART TIMER
// --------------------------------------------

let newGlobalInterval = setInterval(function (){
    startedIntervalMilliSeconds.splice(0, 1, new Date().getTime())
    next()
}, 10000)
clearInterval(newGlobalInterval)

// --------------------------------------------
// EVENTS ON CLICK
// --------------------------------------------


nextPhotoButton.addEventListener('click', function (){
    startedIntervalMilliSeconds.splice(0, 1, new Date().getTime())

    if (photoStopped[0] === false){
        clearInterval(globalInterval)
        let nextPhotoRightNow = setTimeout(next, 0)
        clearInterval(newGlobalInterval)

        newGlobalInterval = setInterval(function (){
            startedIntervalMilliSeconds.splice(0, 1, new Date().getTime())
            next()
        }, 10000)    }
})

previousPhotoButton.addEventListener('click', function (){
    startedIntervalMilliSeconds.splice(0, 1, new Date().getTime())

    if (photoStopped[0] === false){
        let previousPhotoRightNow = setTimeout(previous, 0)
        clearInterval(globalInterval)

        clearInterval(newGlobalInterval)

        newGlobalInterval = setInterval(next, 10000)
    }
})



let ifThereIsNoMoreBreaksNextPhotos1 = setTimeout(function () {})
let ifThereIsNoMoreBreaksNextPhotos2 = setTimeout(function () {})
let ifThereIsNoMoreBreaksNextPhotos3 = setTimeout(function () {})

let breaksCounter = 0
let timeLeftAfterFirstBreak = []
let timeLeftAfterSecondBreakAndMore = []

let newDateInMillisecondsAfterFirstBreak = []


window.addEventListener("scroll", function (){

    const {scrollTop, clientHeight} = document.documentElement 

    // --------------------------------------------------------
    // --------------------------------------------------------
    // START / PAUSE ANIMATION (IMAGES + PROGRESS BAR) ON COVER
    // --------------------------------------------------------
    // --------------------------------------------------------

    if (scrollTop<=696){
        if (flagAnimScroll === true){
            flagAnimScroll = false
            flagAnimScroll_DoubleCheck = false

            progressBar.style.animationPlayState = "running"
            Image1.style.animationPlayState = "running"
            Image2.style.animationPlayState = "running"
            Image3.style.animationPlayState = "running"


            if (breaksCounter<2){
                timeLeftAfterFirstBreak.push(10000 + (startedIntervalMilliSeconds[0] - pausedIntervalMilliSeconds[0]))
            }

            newDateInMillisecondsAfterFirstBreak.push(new Date().getTime())

            function controllAnimationPlayPause(){
                if (breaksCounter === 1){
                    ifThereIsNoMoreBreaksNextPhotos1 = setTimeout(function (){
                        next()
        
                        newGlobalInterval = setInterval(function (){
                            next()
                        }, 10000)

                    }, timeLeftAfterFirstBreak[0])
                }

                if (breaksCounter === 2){
                    ifThereIsNoMoreBreaksNextPhotos2 = setTimeout(function (){
                        next()

                        newGlobalInterval = setInterval(function (){
                            next()
                        }, 10000)

                    }, timeLeftAfterSecondBreakAndMore[1])
                }

                if (breaksCounter >2){
                    ifThereIsNoMoreBreaksNextPhotos3 = setTimeout(function (){
                        next()
        
                        newGlobalInterval = setInterval(function (){
                            next()
                        }, 10000)

                    }, timeLeftAfterSecondBreakAndMore[breaksCounter - 1])
                }    
            }

            controllAnimationPlayPause()

        }
    }

    if (scrollTop > 696){

        let flagAnimScroll = true

        if (flagAnimScroll_DoubleCheck === false){
            flagAnimScroll_DoubleCheck = true

            if (breaksCounter<2){
                if (breaksCounter === 1){   
                    clearTimeout(ifThereIsNoMoreBreaksNextPhotos1)
                } 
                breaksCounter++

                pausedIntervalMilliSeconds.splice(0, 1, new Date().getTime())

                timeLeftAfterSecondBreakAndMore.push(timeLeftAfterFirstBreak[0] + (newDateInMillisecondsAfterFirstBreak[0] - pausedIntervalMilliSeconds[0]))

            }else if (breaksCounter>1){
                if (breaksCounter === 2){
                    clearTimeout(ifThereIsNoMoreBreaksNextPhotos2)
                }
                if (breaksCounter >2){   
                    clearTimeout(ifThereIsNoMoreBreaksNextPhotos3)
                }
                pausedIntervalMilliSeconds.splice(0, 1, new Date().getTime())

                timeLeftAfterSecondBreakAndMore.push(timeLeftAfterSecondBreakAndMore[breaksCounter-1] + (newDateInMillisecondsAfterFirstBreak[breaksCounter - 1] - pausedIntervalMilliSeconds[0]))

                breaksCounter++
            }
        }

        clearInterval(globalInterval)
        clearInterval(newGlobalInterval)

        Image1.style.animationPlayState = "paused"
        Image2.style.animationPlayState = "paused"
        Image3.style.animationPlayState = "paused"

        progressBar.style.animationPlayState = "paused"
    }

})






// --------------------------------------------
// --------------------------------------------
// PLAY/PAUSE ANIMATION WHEN USER GO ON ANOTHER WEBPAGE
// --------------------------------------------
// --------------------------------------------



// window.addEventListener("visibilitychange", function (){
//     if (document.visibilityState === "visible"){
//         progressBar.style.animationPlayState = "running"
//     } else {
//         progressBar.style.animationPlayState = "paused"
//     }
// })