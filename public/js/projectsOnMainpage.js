// --------------------------------------------
// PROJECTS ANIMATIONS
// --------------------------------------------
let projectsOnLeft = document.querySelector(".container_projects .project:nth-child(1)")
let projectsOnMiddle = document.querySelector(".container_projects .project:nth-child(2)")
let projectsOnRight = document.querySelector(".container_projects .project:nth-child(3)")

let bolleanToPreventInfiniteDeletions = false
let bolleanToPreventInfiniteDeletions2 = false
let preventProjectsOnMiddleToBeNullAfterTheEndOfAnimations = false

if (window.matchMedia("(min-width: 481px)").matches){
    projectsOnMiddle.style.top = "50px"
    projectsOnMiddle.style.opacity = "0"
    
    projectsOnLeft.style.left = "-50px"
    projectsOnLeft.style.opacity = "0"
    
    projectsOnRight.style.right = "-50px"
    projectsOnRight.style.opacity = "0"
}



let projects = document.querySelectorAll(".project")


let indexForAllProjects = 1

let numberOfTheFirstOnesMiddleProjectsForCalculateAllProjects = 2
let numberOfTheFirstOnesLeftProjectsForCalculateAllProjects = 1
let numberOfTheFirstOnesRightProjectsForCalculateAllProjects = 3


let preventProjectsOnRightToBeNull = true
let preventProjectsOnMiddleToBeNull = true

// --------------------------------------------------------
// --------------------------------------------------------
// START ANIMATION ON PROJECTS
// --------------------------------------------------------
// --------------------------------------------------------
window.addEventListener("scroll", function (){
    const {scrollTop, clientHeight} = document.documentElement

    if (window.matchMedia("(min-width: 481px)").matches) {
        const topElementToTopViewPort = projectsOnLeft.getBoundingClientRect().top
    
        if (scrollTop>(scrollTop + topElementToTopViewPort).toFixed() - clientHeight*0.95){
    
            projectsOnMiddle.classList.add("activeAnimationProjectOnMiddle")
            projectsOnLeft.classList.add("activeAnimationProjectOnLeft")
            projectsOnRight.classList.add("activeAnimationProjectOnRight")
    
            if (bolleanToPreventInfiniteDeletions === false){
                bolleanToPreventInfiniteDeletions = true
    
                allProjectsOnMiddle.shift()
                allProjectsOnLeft.shift()
                allProjectsOnRight.shift()
            }
            
            if (preventProjectsOnMiddleToBeNullAfterTheEndOfAnimations === false){
    
                let projectsOnMiddle2 = document.querySelector(".container_projects .project:nth-child("+ allProjectsOnMiddle[0] +")")
                let projectsOnLeft2 = document.querySelector(".container_projects .project:nth-child("+ allProjectsOnLeft[0] +")")
                let projectsOnRight2 = document.querySelector(".container_projects .project:nth-child("+ allProjectsOnRight[0] +")")
    
    
                const topElementToTopViewPort2 = projectsOnLeft2.getBoundingClientRect().top
    
                if (preventProjectsOnMiddleToBeNull === true){
                    projectsOnMiddle2.style.top = "50px"
                    projectsOnMiddle2.style.opacity = "0"
                }
    
    
                projectsOnLeft2.style.left = "-50px"
                projectsOnLeft2.style.opacity = "0"
    
                if (preventProjectsOnRightToBeNull === true){
                    projectsOnRight2.style.right = "-50px"
                    projectsOnRight2.style.opacity = "0"
                }
    
        
    
                if(scrollTop>(scrollTop + topElementToTopViewPort2).toFixed() - clientHeight*0.95){
                    if (preventProjectsOnMiddleToBeNull === true){
                        projectsOnMiddle2.classList.add("activeAnimationProjectOnMiddle")
                    }
                    projectsOnLeft2.classList.add("activeAnimationProjectOnLeft")
                    if (preventProjectsOnRightToBeNull === true){
                        projectsOnRight2.classList.add("activeAnimationProjectOnRight")
                    }
    
        
                    if (bolleanToPreventInfiniteDeletions2 === false){
        
                        allProjectsOnMiddle.shift()
                        allProjectsOnLeft.shift()
                        allProjectsOnRight.shift()
    
                        if (allProjectsOnMiddle.length===1){
                            preventProjectsOnMiddleToBeNull = false
                        }
                        if (allProjectsOnRight.length === 1){
                            preventProjectsOnRightToBeNull = false
                        }
    
                        bolleanToPreventInfiniteDeletions2 = true
                        bolleanToPreventInfiniteDeletions2 = false
                        
                        if (allProjectsOnLeft.length===1){
                            preventProjectsOnMiddleToBeNullAfterTheEndOfAnimations = true
                        }
                    }
                }
            }
        }
        
    } 

})



let allProjectsOnMiddle = []
let allProjectsOnLeft = []
let allProjectsOnRight = []

allProjectsOnMiddle.push(numberOfTheFirstOnesMiddleProjectsForCalculateAllProjects)
allProjectsOnLeft.push(numberOfTheFirstOnesLeftProjectsForCalculateAllProjects)
allProjectsOnRight.push(numberOfTheFirstOnesRightProjectsForCalculateAllProjects)



for (project of projects){
    if (indexForAllProjects===numberOfTheFirstOnesMiddleProjectsForCalculateAllProjects){
        
        project.style.position = "relative"

        numberOfTheFirstOnesMiddleProjectsForCalculateAllProjects = numberOfTheFirstOnesMiddleProjectsForCalculateAllProjects + 3

        allProjectsOnMiddle.push(numberOfTheFirstOnesMiddleProjectsForCalculateAllProjects)
    }

    if (indexForAllProjects ===numberOfTheFirstOnesLeftProjectsForCalculateAllProjects){
        project.style.position = "relative"

        numberOfTheFirstOnesLeftProjectsForCalculateAllProjects = numberOfTheFirstOnesLeftProjectsForCalculateAllProjects + 3

        allProjectsOnLeft.push(numberOfTheFirstOnesLeftProjectsForCalculateAllProjects)
    }

    if (indexForAllProjects ===numberOfTheFirstOnesRightProjectsForCalculateAllProjects){
        project.style.position = "relative"
    // project.style.backgroundColor = "red"


        numberOfTheFirstOnesRightProjectsForCalculateAllProjects = numberOfTheFirstOnesRightProjectsForCalculateAllProjects + 3

        allProjectsOnRight.push(numberOfTheFirstOnesRightProjectsForCalculateAllProjects)
    }

    indexForAllProjects++

}



let selectAll = document.querySelectorAll(".project")

let indexOfAllProjects = 1
let allProjects = []

let blockBugInfiniteAnimation = true


if (window.matchMedia("(min-width: 481px)").matches){
    for (selected of selectAll){

        allProjects.push(indexOfAllProjects)
    
        selected.addEventListener('mouseenter', function(){
    
            if (blockBugInfiniteAnimation === true){
                for (index of allProjects){
                    // blur(index)
        
        
                    // this.style.filter = "blur(0px)"
                    // this.style.boxShadow = "0px 4px 11px 1px black"
                    this.style.opacity = "1"
                    this.style.left = "0px"
                    this.style.right = "0px"
                    this.style.top = "0px"
                    this.classList.remove("activeAnimationProjectOnMiddle")
                    this.classList.remove("activeAnimationProjectOnLeft")
                    this.classList.remove("activeAnimationProjectOnRight")
                    this.classList.remove("disableAnimationProjectScale")
        
                    this.classList.add("activeAnimationProjectScale")
        
                    blockBugInfiniteAnimation = false
            
                }
            }
        })
        indexOfAllProjects++
    }
    
    
    for (selected of selectAll){
    
        selected.addEventListener('mousemove', function(){
            if (blockBugInfiniteAnimation === true){
                for (index of allProjects){
                    blockBugInfiniteAnimation = false
        
                    // this.style.filter = "blur(0px)"
                    // this.style.boxShadow = "0px 4px 11px 1px black"
                    this.style.opacity = "1"
                    this.style.left = "0px"
                    this.style.right = "0px"
                    this.style.top = "0px"
                    this.classList.remove("activeAnimationProjectOnMiddle")
                    this.classList.remove("activeAnimationProjectOnLeft")
                    this.classList.remove("activeAnimationProjectOnRight")
                    this.classList.remove("disableAnimationProjectScale")
        
                    this.classList.add("activeAnimationProjectScale")
                }
            }
        })
        indexOfAllProjects++
    }
    
    
    for (selected of selectAll){
        selected.addEventListener('mouseleave', function(){
    
            setTimeout(function (){
                blockBugInfiniteAnimation = true
            }, 10)   
    
           this.style.boxShadow = "0px 0px 0px 0px"
           this.classList.remove("activeAnimationProjectScale")
        })
    
    }

}