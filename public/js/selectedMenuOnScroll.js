// --------------------------------------------
// --------------------------------------------
// SELECTED MENU
// --------------------------------------------
// --------------------------------------------
let selectedSectionNavbar = document.querySelectorAll('.container_menu')

let bordersNavbar = document.querySelectorAll(".border_bottom")
let menuElementsNavbar = document.querySelectorAll(".menu_elements")

// SELECTORS FOR SELECTED MENU SCROLL
let projettitle = document.querySelector(".projettitle")
let competencetitle = document.querySelector(".competencetitle")
let apropostitle = document.querySelector(".apropostitle")
let contacttitle = document.querySelector(".contacttitle")


function selectedMenu(child){
    // remove all style before adding selected child
    for (border of bordersNavbar){
        border.classList.remove("selected_navbar")
    }

    for (menu of menuElementsNavbar){
        menu.classList.remove("selected_menu_elements")

    }


    let selectedBorder = document.querySelector(".navbar .container_menu:nth-child(" + child + ") .border_bottom") 
    let selectedText = document.querySelector(".navbar .container_menu:nth-child(" + child + ") .menu_elements") 

    selectedText.classList.add("selected_menu_elements")
    selectedBorder.classList.add("selected_navbar")
}

let blockAnim = true

// set a blokcker to avoid multiple selected menu when clicking (due to scroll : indeed, if we click on last section while we are on first section, section 2,3,4 etc will selected to finish by the section we wanted)
for (container of selectedSectionNavbar){
    container.addEventListener('click', function(){
        function addSelectedMenuOnIOS(){
            for (border of bordersNavbar){
                border.classList.remove("selected_navbar")
            }
        
            for (menu of menuElementsNavbar){
                menu.classList.remove("selected_menu_elements")
            }
    
            this.firstChild.nextSibling.nextSibling.nextSibling.classList.add("selected_menu_elements")
            this.firstChild.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.classList.add("selected_navbar")
        }
        
        addSelectedMenuOnIOS()
    })
}


// set child that will used in selectedMenu function in each title sections

let getSelectedChild = 1
let titlesSection = document.querySelectorAll(".title")

for (title of titlesSection){
    title.dataset.order = getSelectedChild
    getSelectedChild+=2
}

window.addEventListener("scroll", function (){
    const {scrollTop, clientHeight} = document.documentElement

    const top_projet_titleToTopViewPort = projettitle.getBoundingClientRect().top
    const top_competence_titleToTopViewPort = competencetitle.getBoundingClientRect().top
    const top_apropos_titleToTopViewPort = apropostitle.getBoundingClientRect().top
    const top_contact_titleToTopViewPort = contacttitle.getBoundingClientRect().top

    let projet_title = (scrollTop + top_projet_titleToTopViewPort).toFixed() - clientHeight*0.3
    let competence_title = (scrollTop + top_competence_titleToTopViewPort).toFixed() - clientHeight*0.3
    let apropos_title = (scrollTop + top_apropos_titleToTopViewPort).toFixed() - clientHeight*0.3
    let contact_title = (scrollTop + top_contact_titleToTopViewPort).toFixed() - clientHeight*0.7

    if (scrollTop < projet_title){
        for (border of bordersNavbar){
            border.classList.remove("selected_navbar")
        }
    
        for (menu of menuElementsNavbar){
            menu.classList.remove("selected_menu_elements")
        }

    }

    if (scrollTop > projet_title & scrollTop < competence_title){
        selectedMenu(projettitle.dataset.order)
    }

    if (scrollTop > competence_title & scrollTop < apropos_title){
        selectedMenu(competencetitle.dataset.order)
    }

    if (scrollTop > apropos_title & scrollTop < contact_title){
        selectedMenu(apropostitle.dataset.order)
    }

    if (scrollTop > contact_title){
        selectedMenu(contacttitle.dataset.order)
    }

})
