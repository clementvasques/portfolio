// ---------------- PONG -------------------

//  TODO :
//  Planche qui dépasse parfois du jeux (le probleme vient de la vitesse de l'intervale, il faut faire un calcul de distance entre la planche et le bord du jeux)
//  Si la partie est terminée on peut continuer à jouer
//  Ajouter un bouton pause 


if (window.matchMedia("(min-width: 481px)").matches){

    function randomNumber(min, max){
        if (typeof min !== "number" || isNaN(min)){
            return
        } else {
            let random = Math.floor(Math.random() * (max-min)) + min
            // evite que la balle soit parfaitement alignée dans l'axe Y au démarrage de la partie 
            if (random === 0){
                random = 1
            }
            return random
        }
    }
    
    let ball = document.querySelector(".ball")
    let scorePlayer1 = document.querySelector(".scorePlayer1")
    let scorePlayer2 = document.querySelector(".scorePlayer2")
    let planchePlayer1 = document.querySelector(".planchePlayer1")
    let planchePlayer2 = document.querySelector(".planchePlayer2")
    let winnerEcriture = document.querySelector(".winnerEcriture")
    let numeroPlayerWinner = document.querySelector('.numeroPlayerWinner')
    let replay = document.querySelector(".replay")
    let box = document.querySelector(".box")
    
    // caches les blocs de "victoire"
    winnerEcriture.style.display = 'none'
    replay.style.display = "none"
    
    // nombre de pixels de déplacements (vitesse)
    let positionX = ball.offsetLeft
    positionX += 0
    let positionY = ball.offsetTop
    positionY +=0
    
    let positionPlanche1 = planchePlayer1.offsetTop
    positionPlanche1=0
    let positionPlanche1X = planchePlayer1.offsetLeft
    
    let positionPlanche2 = planchePlayer2.offsetTop
    positionPlanche2=0
    let positionPlanche2X = planchePlayer2.offsetLeft
    
    
    
    
    let compteurScorePlayer1 = 1
    let compteurScorePlayer2 = 1
    
    let compteurScoreP1 = 0
    let compteurScoreP2 = 0
    
    let limitDown = box.clientHeight - planchePlayer1.clientHeight
    
    let largeurBox = box.clientWidth
    let hauteur = box.clientHeight
    
    let centreLargeur = largeurBox / 2 - ball.clientWidth/2
    let centreHauteur = hauteur / 2 - (ball.clientWidth / 2)
    
    // balle au centre
    ball.style.top = centreHauteur + "px"
    ball.style.left = centreLargeur + "px"
    
    
    
    
    let moovePlanchePlayer = setInterval(function() { 
        planchePlayer1.style.top = (planchePlayer1.offsetTop + positionPlanche1) + "px"
        planchePlayer2.style.top = planchePlayer2.offsetTop + positionPlanche2 + "px"
    }, 16);
    
    
    setInterval(function() {
        if  (ball.offsetLeft > largeurBox - planchePlayer1.clientWidth ){
            compteurScoreP1 = 1
            scorePlayer1.textContent = compteurScorePlayer1++
            positionX = -15
        } else if (ball.offsetLeft<0){
            compteurScoreP2 = 1
            scorePlayer2.textContent = compteurScorePlayer2++
            positionX = 15
        } 
        ball.style.left = ball.offsetLeft + positionX + "px"
    
        if  (ball.offsetTop> hauteur - ball.clientWidth){
            positionY = -8
        } else if (ball.offsetTop<0){
            positionY = 8
        } 
        ball.style.top = ball.offsetTop + positionY + "px"
    
        let positionSquareAxeY = ball.offsetTop + positionY
        let positionSquareAxeX = ball.offsetLeft + positionX
    
        let positionPlanche1AxeY = planchePlayer1.offsetTop + positionPlanche1
        let positionPlanche2AxeY = planchePlayer2.offsetTop + positionPlanche2 
    
        // Lorsque la balle rebondit sur les planches
        if (positionSquareAxeY>positionPlanche1AxeY && positionSquareAxeY<positionPlanche1AxeY + 100 && positionSquareAxeX<positionPlanche1X + 17){
            positionX = randomNumber(8, 11)
            positionY = randomNumber(-8, 11)
        }
        if (positionSquareAxeY>positionPlanche2AxeY && positionSquareAxeY<positionPlanche2AxeY + 100 && positionSquareAxeX>positionPlanche2X - 17){
            positionX = randomNumber(-11, -8)
            positionY = randomNumber(-11, 8)
        }
    
        // balle au centre lorsqu'elle touche un des deux camps par rapport au flag compteur score
        if (compteurScoreP1 === 1){
            positionX = 0  
            positionY = 0
            compteurScoreP1--
            ball.style.top = centreHauteur + "px"
            ball.style.left = centreLargeur + "px"
        }
        if (compteurScoreP2 === 1){
            positionX = 0  
            positionY = 0
            compteurScoreP2--
            ball.style.top = centreHauteur + "px"
            ball.style.left = centreLargeur + "px"
    
        }
    
        // que faire lorsqu'un joueur gagne
        if (scorePlayer1.textContent=== "3"){
            winnerEcriture.style.display = 'flex'
            numeroPlayerWinner.textContent = "Player 1 WIN !"
            replay.style.display = "block"
        } else if (scorePlayer2.textContent==="3"){
            winnerEcriture.style.display = "flex"
            numeroPlayerWinner.textContent = "Player 2 WIN !"
            replay.style.display = "block"
        }
    }, 16 );
    
    
    document.addEventListener('keypress', function(space){
        if (space.code == "Space"){
                positionX = randomNumber(-8, 8)
                positionY = randomNumber(-8, 6)
                space.preventDefault()
        }
    })
    
    document.addEventListener('keydown', function(downS){
        if (downS.code == "KeyX"){
    
            positionPlanche1 = 10
        }
        if (planchePlayer1.offsetTop>limitDown){
            positionPlanche1 = 0
        }
    })
    
    document.addEventListener('keydown', function(downZ){
        if (downZ.code == "KeyW"){
            positionPlanche1 = -10
            if (planchePlayer1.offsetTop<6){
                positionPlanche1 = 0
            } 
        }
    })
    
    document.addEventListener('keydown', function(downL){
        if (downL.code == "KeyN"){
            positionPlanche2 = 10
        }
        if (planchePlayer2.offsetTop> hauteur - planchePlayer2.clientHeight){
            positionPlanche2 = 0
        }
    })
    
    document.addEventListener('keydown', function(downO){
        if (downO.code == "KeyI"){
            positionPlanche2 = -10
            if (planchePlayer2.offsetTop<6){
                positionPlanche2 = 0
            }
        }
    })
    
    document.addEventListener('keyup', function(relache){
        if (relache.code == "KeyX" || relache.code == "KeyW"){
            positionPlanche1 = 0
        }
        if (relache.code == "KeyI" || relache.code == "KeyN"){
            positionPlanche2 = 0
        }
    })
    
    replay.addEventListener('click', function(){
        scorePlayer2.textContent = "0"
        scorePlayer1.textContent = "0"
        compteurScorePlayer1 = 1
        compteurScorePlayer2 = 1
    
        winnerEcriture.style.display = 'none'
        replay.style.display = "none"
    
    
        ball.style.top = centreHauteur + "px"
        ball.style.left = centreLargeur + "px"
        planchePlayer1.style.top = 28 + "vh"
        planchePlayer2.style.top = 28 + "vh"
        positionX = 3
        positionY = 3
    })

}







