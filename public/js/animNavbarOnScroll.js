let logoNavbar = document.querySelector(".container_logo")
let contactHeader = document.querySelector(".contact_header")
let navbar = document.querySelector(".navbar")
let navBackground = document.querySelector(".nav_background")

let flagNavbar = undefined
let flagAnimScroll = undefined



let flagAnimScroll_DoubleCheck = false


window.addEventListener("scroll", function (){
    const {scrollTop, clientHeight} = document.documentElement

    if (scrollTop <= 40){
        if (flagNavbar === true){
            navBackground.style.top = "-5px"
            navBackground.style.transition = "0.2s ease-in"
            navBackground.style.opacity = "0.4"


            navbar.style.animation = "anim_text_navbar 0.3s forwards"

            if (window.matchMedia("(max-width: 481px)").matches){

                
                
            } else {
                logoNavbar.style.top = "0px"


            }

            flagNavbar = false
        }
    }

    if (scrollTop > 40){
        flagNavbar = true
        navbar.style.right = "0px"
        navbar.style.paddingRight = "10vw"
        navbar.style.transform = "translatey(60px)"
        navbar.style.transition = "ease-in 0.2s"
        navbar.style.animationDelay = "0.5s"
        navbar.style.transition = "ease-in 0.2s"
        navbar.style.position = "fixed"
        navbar.style.top = "-60px"
        navbar.style.right = "0px"


        
        navBackground.style.transform = "translatey(40px)"
        navBackground.style.transition = "ease-in 0.2s"
        navBackground.style.animationDelay = "0.5s"
        navBackground.style.transition = "ease-in 0.2s"
        navBackground.style.position = "absolute"
        navBackground.style.top = "-40px"
        navBackground.style.position = "fixed"
        navBackground.style.opacity = "0.9"
        navBackground.style.boxShadow = "0px 5px 19px -11px"

        navbar.style.animation = "unset"

        if (window.matchMedia("(max-width: 481px)").matches){

            logoNavbar.style.position = "fixed"
            
            
        } else {
            logoNavbar.style.transform = "translatey(60px)"
            logoNavbar.style.transition = "ease-in 0.2s"
            logoNavbar.style.animationDelay = "0.5s"
            logoNavbar.style.transition = "ease-in 0.2s"
            logoNavbar.style.position = "fixed"
            logoNavbar.style.top = "-35px"
        }
    }
})