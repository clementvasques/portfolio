// ----------- SKILLS -----------
// ----------- SKILLS -----------
// ----------- SKILLS -----------
let skills_dev = document.querySelectorAll(".skill_dev")
let skills_webdesign = document.querySelectorAll(".skill_webdesign")



for (skill of skills_dev){
    skill.style.width = "0%"
}

for (skill of skills_webdesign){
    skill.style.width = "0%"
}

let totalNumberSkillsDev = skills_dev.length
let totalNumberSkillsWebdesign = skills_webdesign.length

if (window.matchMedia("(min-width: 481px)").matches){

    window.addEventListener("scroll", function (){
        const {scrollTop, clientHeight} = document.documentElement
    
        let skill_dev_container = this.document.querySelector(".skills_development")
        let skill_webdesign_container = this.document.querySelector(".skills_webdesign")
        let container_skills_collumn_right = this.document.querySelector(".container_skills_collumn_right")
    
        const topSkillDevToTopViewPort = skill_dev_container.getBoundingClientRect().top
        const topSkillWebdesignToTopViewPort = skill_webdesign_container.getBoundingClientRect().top
    
    
        if (scrollTop>(scrollTop + topSkillDevToTopViewPort).toFixed() - clientHeight*0.8){
            skill_dev_container.classList.add("anim_Container_Skills")
            container_skills_collumn_right.classList.add("anim_Container_Skills_Right")
    
            if (blockInfiniteAnimation === true){
                animation(totalNumberSkillsDev)
                blockInfiniteAnimation = false
            }
    
        }
    
        if (scrollTop>(scrollTop + topSkillWebdesignToTopViewPort).toFixed() - clientHeight*0.8){
            skill_webdesign_container.classList.add("anim_Container_Skills")
    
            if (blockInfiniteAnimation2 === true){
                animation(totalNumberSkillsWebdesign)
                blockInfiniteAnimation2 = false
            }
        }
    
    })
    
    let blockInfiniteAnimation = true
    let blockInfiniteAnimation2 = true
    
    
    function animation(param)
    {
        
        let animDelayMilliseconds = 150
        
        let totalNumberSkillToAnimate = param
        
        let i = 0
        let incrementSelector = 0
        
        function give_Me_Width_Score(value){
            let calculation = value * 10
        
            return calculation
        }
        
        setTimeout(function() { 
            display(i) 
        }, 1 * i);
        
        function display(i)
        {
            incrementSelector+=2
        
            if (totalNumberSkillsDev === param){
                let skillSelectedDev = document.querySelector(".container_global_skill .container_skill:nth-child("+ incrementSelector +") .skill_dev")
                let datascore = skillSelectedDev.dataset.score
        
                skillSelectedDev.style.width = give_Me_Width_Score(datascore) + "%"
                skillSelectedDev.style.animation = "skillAnimation 0.4s ease-in"
                skillSelectedDev.style.transform = "translate()"
        
            }
        
            if (totalNumberSkillsWebdesign === param){
                let skillSelectedWebdesign = document.querySelector(".container_global_skill :nth-child("+ incrementSelector +") .skill_webdesign")
            
                let datascoreWebdesign = skillSelectedWebdesign.dataset.score
        
                skillSelectedWebdesign.style.width = give_Me_Width_Score(datascoreWebdesign) + "%"
                skillSelectedWebdesign.style.animation = "skillAnimation 0.4s ease-in"
                skillSelectedWebdesign.style.transform = "translate()"
            }
        
            i++;
        
            if(i < totalNumberSkillToAnimate){
                setTimeout(function() { 
                    display(i) 
                }, animDelayMilliseconds);
            }
        
        }
    }

} else {
    window.addEventListener("scroll", function (){
        const {scrollTop, clientHeight} = document.documentElement
    
        let skill_dev_container = this.document.querySelector(".skills_development")
        let skill_webdesign_container = this.document.querySelector(".skills_webdesign")
        let soft_skill = this.document.querySelector(".container_soft_skill")
        let bonus_skill = this.document.querySelector(".container_bonus_skill")
        let large = this.document.querySelector(".large_container_skills")
    
        const topSkillDevToTopViewPort = skill_dev_container.getBoundingClientRect().top
        const topSkillWebdesignToTopViewPort = skill_webdesign_container.getBoundingClientRect().top
        const topSkillSoftSkillToTopViewPort = soft_skill.getBoundingClientRect().top
        const topSkillBonusSKillToTopViewPort = bonus_skill.getBoundingClientRect().top
    
        if (scrollTop>(scrollTop + topSkillDevToTopViewPort).toFixed() - clientHeight*0.6){
            skill_dev_container.classList.add("anim_Container_Skills")

    
            if (blockInfiniteAnimation === true){
                animation(totalNumberSkillsDev)
                blockInfiniteAnimation = false
            }
    
        }
    
        if (scrollTop>(scrollTop + topSkillWebdesignToTopViewPort).toFixed() - clientHeight*0.6){
            skill_webdesign_container.classList.add("anim_Container_Skills")
    
            if (blockInfiniteAnimation2 === true){
                animation(totalNumberSkillsWebdesign)
                blockInfiniteAnimation2 = false
            }
        }

        if (scrollTop>(scrollTop + topSkillSoftSkillToTopViewPort).toFixed() - clientHeight*0.6){
            soft_skill.classList.add("anim_Container_Skills_Right")
        }

        if (scrollTop>(scrollTop + topSkillBonusSKillToTopViewPort).toFixed() - clientHeight*0.6){
            bonus_skill.classList.add("anim_Container_Skills_Right")
        }
    })
    
    let skills_dev = document.querySelectorAll(".skill_dev")
    let skills_webdesign = document.querySelectorAll(".skill_webdesign")
    
    
    
    for (skill of skills_dev){
        skill.style.width = "0%"
    }
    
    for (skill of skills_webdesign){
        skill.style.width = "0%"
    }
    
    let totalNumberSkillsDev = skills_dev.length
    let totalNumberSkillsWebdesign = skills_webdesign.length
    
    
    let blockInfiniteAnimation = true
    let blockInfiniteAnimation2 = true
    
    
    function animation(param)
    {
        let animDelayMilliseconds = 150
        
        
        let totalNumberSkillToAnimate = param
        
        let i = 0
        let incrementSelector = 0
        
        function give_Me_Width_Score(value){
            let calculation = value * 10
        
            return calculation
        }
        
        setTimeout(function() { 
            display(i) 
        }, 1 * i);
        
        function display(i)
        {
            incrementSelector+=2
        
            if (totalNumberSkillsDev === param){
                let skillSelectedDev = document.querySelector(".container_global_skill .container_skill:nth-child("+ incrementSelector +") .skill_dev")
                let datascore = skillSelectedDev.dataset.score
        
                skillSelectedDev.style.width = give_Me_Width_Score(datascore) + "%"
                skillSelectedDev.classList.add("skillAnimation")
        
            }
        
            if (totalNumberSkillsWebdesign === param){
                let skillSelectedWebdesign = document.querySelector(".container_global_skill :nth-child("+ incrementSelector +") .skill_webdesign")
            
                let datascoreWebdesign = skillSelectedWebdesign.dataset.score
        
                skillSelectedWebdesign.style.width = give_Me_Width_Score(datascoreWebdesign) + "%"
                skillSelectedWebdesign.classList.add("skillAnimation")

                // skillSelectedWebdesign.style.animation = "skillAnimation 0.4s ease-in"
                // skillSelectedWebdesign.style.transform = "translate()"
            }
        
            i++;
        
            if(i < totalNumberSkillToAnimate){
                setTimeout(function() { 
                    display(i) 
                }, animDelayMilliseconds);
            }
        
        }
    }

}
