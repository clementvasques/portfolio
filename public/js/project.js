if (window.matchMedia("(max-width: 480px)").matches){
    let seeMoreContent = document.querySelector(".see_more")
    let contentCut = document.querySelector(".contentCut")
    let contentEntire = document.querySelector(".contentEntire")

    seeMoreContent.addEventListener("click", function(){
        contentCut.style.display = "none"
        contentEntire.style.display = "block"
        contentEntire.style.paddingTop = "60px"
    })
}
